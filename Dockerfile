FROM node:10.21.0-alpine3.11

WORKDIR ./app

COPY package*.json ./

RUN yarn install

ENV PATH ./app/node_modules/.bin:$PATH

COPY . .

EXPOSE 3000

CMD ["yarn", "start"]
